/* Copyright (c) 2012-2017,2021 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __QAVB_LIB_H__
#define __QAVB_LIB_H__

#include "eavb_shared.h"

typedef enum avb_role avb_role_t;
typedef enum stream_mapping_type stream_mapping_type_t;
typedef enum ring_buffer_mode ring_buffer_mode_t;
typedef enum avb_class avb_class_t;
typedef enum data_endianness data_endianness_t;
typedef enum avb_ieee1722_version avb_ieee1722_version_t;
typedef enum avtp_crf_mode avtp_crf_mode_t;
typedef enum avtp_crf_type avtp_crf_type_t;
typedef enum avtp_crf_pull avtp_crf_pull_t;

typedef struct eavb_ioctl_hdr eavb_ioctl_hdr_t;
typedef struct eavb_ioctl_stream_config eavb_ioctl_stream_config_t;
typedef struct eavb_ioctl_create_stream eavb_ioctl_create_stream_t;
typedef struct eavb_ioctl_create_stream_with_path eavb_ioctl_create_stream_with_path_t;
typedef struct eavb_ioctl_stream_info eavb_ioctl_stream_info_t;
typedef struct eavb_ioctl_get_stream_info eavb_ioctl_get_stream_info_t;
typedef struct eavb_ioctl_connect_stream eavb_ioctl_connect_stream_t;
typedef struct eavb_ioctl_buf_hdr eavb_ioctl_buf_hdr_t;
typedef struct eavb_ioctl_buf_data eavb_ioctl_buf_data_t;
typedef struct eavb_ioctl_receive eavb_ioctl_receive_t;
typedef struct eavb_ioctl_recv_done eavb_ioctl_recv_done_t;
typedef struct eavb_ioctl_transmit eavb_ioctl_transmit_t;
typedef struct eavb_ioctl_disconnect_stream eavb_ioctl_disconnect_stream_t;
typedef struct eavb_ioctl_destroy_stream eavb_ioctl_destroy_stream_t;

typedef eavb_ioctl_hdr_t		qavb_handler;
typedef eavb_ioctl_stream_info_t	qavb_stream_info;
typedef eavb_ioctl_buf_data_t		qavb_buf;

/**
 * @brief Parses the input configuration file and creates talker/listener thread
 *        and initializes the streamCtx pointer for QAVB stream
 *
 * @param cfgfilepath : location of config file
 * @param streamCtx : [out] Pointer to qavb stream context
 *
 * @return 0: Success, streamCtx will be initialized with valid pointer
 *         -1: Error - file not found, streamCtx will be initialized to NULL
 *         -2: Error - insufficient data in config file, streamCtx will be
 *                     initialized to NULL
 **/
int qavb_create_stream_remote(int fd, char* cfgfilepath, qavb_handler* hdr);

/**
 * @brief Tears down talker/listener thread and releases context and resources
 *        allocated for QAVB stream.
 *
 * @param streamCtx : pointer to the avb stream object that was returned
 *                    by qavb_create_stream_remote().
 *
 * @return 0: Success - streamCtx pointer is no longer valid.
 *         non-zero: Failure
 **/
int qavb_destroy_stream(int fd, qavb_handler* hdr);

/**
 * @brief Retrieves stream configuration information.
 *
 * @param streamCtx : pointer to the avb stream object that was returned
 *                    by qavb_create_stream_remote().
 * @param info: Pointer to a qavb_stream_info struct.
 *
 * @return 0: Success - information copied into the given qavb_stream_info struct
 *         non-zero: Failure
 **/
int qavb_get_stream_info(int fd, qavb_handler* hdr, qavb_stream_info* info);

/**
 * @brief Starts qAVB stream
 *
 * @param streamCtx : pointer to the avb stream object that was returned
 *                    by qavb_create_stream_remote().
 *
 * @return 0: Success
 *         non-zero: Failure
 **/
int qavb_connect_stream(int fd, qavb_handler* hdr);

/**
 * @brief Stops qAVB stream
 *
 * @param streamCtx : pointer to the avb stream object that was returned
 *                    by qavb_create_stream_remote().
 *
 * @return 0: Success
 *         non-zero: Failure
 **/
int qavb_disconnect_stream(int fd, qavb_handler* hdr);

/**
 * @brief Enqueues data for qAVB stream.
 * Non-blocking: will send as much as it can up to size before returning
 *
 * @param streamCtx : pointer to the avb stream object that was returned
 *                    by qavb_create_stream_remote().
 * @param buff : pointer to data buffer to send
 * @param size : size of data to send
 *
 * @return  >= 0 : Number of bytes sent
 *          < 0  : Failure
 *
 **/
int qavb_transmit(int fd, qavb_handler* hdr, qavb_buf* buff);

/**
 * @brief Reads data for qAVB stream.
 * Non-blocking: will read as much data as available up to size before returning
 *
 * @param streamCtx : pointer to the avb stream object that was returned
 *                    by qavb_create_stream_remote().
 * @param buff : pointer to data buffer to write to
 * @param size : size of data to read
 *
 * @return  >= 0 : Number of bytes read (should be multiple of data type)
 *          < 0  : Failure
 *
 **/
int qavb_receive(int fd, qavb_handler* hdr, qavb_buf* buff);

int qavb_receive_done(int fd, qavb_handler* hdr, qavb_buf* buff);


#endif /*__QAVB_LIB_H__*/

/* Copyright (c) 2021 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include  <stdlib.h>
#include  <string.h>
#include <stdio.h>
#include <time.h>
#include <sched.h>
#include <math.h>
#include <pthread.h>
#include <sys/stat.h>
#include <signal.h>

#include <sys/types.h>
#include <sys/ioctl.h>
#include <stdint.h>

#include "qavblib.h"


int qavb_create_stream_remote(int fd, char* cfgfilepath, qavb_handler* hdr)
{
	int ret = 0;
	eavb_ioctl_create_stream_with_path_t req;

	memset(&req, 0, sizeof(req));
	memcpy(req.path, cfgfilepath, strlen(cfgfilepath)+1);

	ret = ioctl(fd, EAVB_IOCTL_CREATE_STREAM_WITH_PATH, &req, sizeof(req));
	*hdr = req.hdr;

	return ret;
}


int qavb_destroy_stream(int fd, qavb_handler* hdr)
{
	int ret = 0;
	eavb_ioctl_destroy_stream_t req = {
		.hdr = *hdr,
	};
	ret = ioctl(fd, EAVB_IOCTL_DESTROY_STREAM, &req, sizeof(req));

	return ret;
}

int qavb_get_stream_info(int fd, qavb_handler* hdr, qavb_stream_info* info)
{
	int ret = 0;
	eavb_ioctl_get_stream_info_t req = {
		.hdr = *hdr,
	};
	ret = ioctl(fd, EAVB_IOCTL_GET_STREAM_INFO, &req, sizeof(req));
	*info = req.info;

	return ret;
}

int qavb_connect_stream(int fd, qavb_handler* hdr)
{
	int ret = 0;
	eavb_ioctl_connect_stream_t req = {
		.hdr = *hdr,
	};
	ret = ioctl(fd, EAVB_IOCTL_CONNECT_STREAM, &req, sizeof(req));

	return ret;
}

int qavb_disconnect_stream(int fd, qavb_handler* hdr)
{
	int ret = 0;
	eavb_ioctl_disconnect_stream_t req = {
		.hdr = *hdr,
	};
	ret = ioctl(fd, EAVB_IOCTL_DISCONNECT_STREAM, &req, sizeof(req));

	return ret;
}

int qavb_transmit(int fd, qavb_handler* hdr, qavb_buf* buff)
{
	int ret = 0;
	eavb_ioctl_transmit_t req = {
		.hdr = *hdr,
		.data = *buff,
	};
	ret = ioctl(fd, EAVB_IOCTL_TRANSMIT, &req, sizeof(req));
	if (0 == ret) {
		return req.written;
	}
	else {
		return ret;
	}
}

int qavb_receive(int fd, qavb_handler* hdr, qavb_buf* buff)
{
	int ret = 0;
	if (NULL == buff)
		return -1;

	eavb_ioctl_receive_t req = {
		.hdr = *hdr,
		.data = *buff,
	};
	ret = ioctl(fd, EAVB_IOCTL_RECEIVE, &req, sizeof(req));

	if (0 == ret) {
		buff->hdr = req.data.hdr;
		return req.received;
	}
	else {
		return ret;
	}
}

int qavb_receive_done(int fd, qavb_handler* hdr, qavb_buf* buff)
{
	int ret = 0;
	if (NULL == buff)
		return -1;

	eavb_ioctl_recv_done_t req = {
		.hdr = *hdr,
		.data = *buff,
	};
	ret = ioctl(fd, EAVB_IOCTL_RECV_DONE, &req, sizeof(req));

	return ret;
}

